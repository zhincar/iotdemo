package com.link510.agent.demo.services;

import com.link510.agent.demo.helper.HttpHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service(value = "CWMUtils")
public class CWMUtils {

    @Autowired
    private AppConfig appConfig;


    public String dpGet(String methodUrl) {
        return dpGet(methodUrl, null);
    }

    public String dpGet(String methodUrl, Map<String, Object> params) {

        String httpUrl = getApiUrl(methodUrl);

        return HttpHelper.doGet(httpUrl, params, getApiHeader());

    }

    private String getApiUrl(String methodUrl) {
        return appConfig.getHttpUrl() + methodUrl;
    }

    private Map<String, String> getApiHeader() {

        Map<String, String> headers = new HashMap<>();
        headers.put("X-CWMAPI-ApiKey", appConfig.getApiKey());
        headers.put("X-CWMAPI-ApiSecret", appConfig.getApiSecret());

        return headers;
    }
}
