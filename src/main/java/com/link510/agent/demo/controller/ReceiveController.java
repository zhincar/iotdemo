package com.link510.agent.demo.controller;


import com.alibaba.fastjson.JSON;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.Size;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "receive")
public class ReceiveController {


    /**
     * GET接收
     *
     * @return
     */
    @RequestMapping(method = RequestMethod.GET)
    public String get(HttpServletRequest request, HttpServletResponse response) {

        Map<String, String[]> maps = request.getParameterMap();

        System.out.println("参数:" + JSON.toJSONString(maps));

        return "成功GET接收";
    }


    /**
     * POST接收
     *
     * @return
     */
    @RequestMapping(method = RequestMethod.POST)
    public String post(HttpServletRequest request, HttpServletResponse response) {

        Map<String, String[]> maps = request.getParameterMap();

        System.out.println("参数:" + JSON.toJSONString(maps));

        return "成功POST接收";
    }

}
