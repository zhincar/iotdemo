package com.link510.agent.demo.controller;


import com.link510.agent.demo.services.CWMUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController(value = "SubscribeController")
@RequestMapping(value = "subscribe")
public class SubscribeController {

    @Autowired
    private CWMUtils cwmUtils;

    /**
     * 消息订阅列表
     *
     * @return
     */
    @RequestMapping(value = "list")
    public String list() {
        return cwmUtils.dpGet("subscribe/list");
    }

    /**
     * 消息订阅申请
     *
     * @return
     */
    @RequestMapping(value = "send")
    public String send() {

        Map<String, Object> params = new HashMap<>();
        params.put("title", "测试消息订阅");
        params.put("productId", 1);
        params.put("protocol", "0XB0");
        params.put("httpUrl", "http://192.168.1.188:8026/receive");

        return cwmUtils.dpGet("subscribe/send", params);
    }

    /**
     * 取消消息订阅
     *
     * @return
     */
    @RequestMapping(value = "cancel")
    public String cannel() {

        Map<String, Object> params = new HashMap<>();
        params.put("productId", 1);
        params.put("protocol", "0XB0");

        return cwmUtils.dpGet("subscribe/cancel", params);
    }

    /**
     * 测试消息订阅
     *
     * @return
     */
    @RequestMapping(value = "test")
    public String test(){

        Map<String, Object> params = new HashMap<>();
        params.put("deviceSN", "23380");
        params.put("protocol", "0XB0");

        return cwmUtils.dpGet("subscribe/test", params);
    }


}
