package com.link510.agent.demo.controller;

import com.link510.agent.demo.services.CWMUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController(value = "DeviceController")
@RequestMapping(value = "device")
public class DeviceController {

    @Autowired
    private CWMUtils cwmUtils;

    /**
     * 列表
     *
     * @return
     */
    @RequestMapping(value = {"list"})
    public String list() {
        return cwmUtils.dpGet("device/list");
    }

    /**
     * 列表
     *
     * @return
     */
    @RequestMapping(value = {"show"})
    public String show(@RequestParam(defaultValue = "23380") String deviceSN) {

        Map<String, Object> params = new HashMap<>();
        params.put("deviceSN", deviceSN);

        return cwmUtils.dpGet("device/show", params);
    }

    /**
     * 列表
     *
     * @return
     */
    @RequestMapping(value = {"product"})
    public String product(@RequestParam(defaultValue = "23380") String deviceSN) {

        Map<String, Object> params = new HashMap<>();
        params.put("deviceSN", deviceSN);

        return cwmUtils.dpGet("device/product", params);
    }

}
