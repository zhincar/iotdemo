package com.link510.agent.demo.controller;

import com.link510.agent.demo.services.CWMUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController(value = "MessageController")
@RequestMapping(value = "message")
public class MessageController {

    @Autowired
    private CWMUtils cwmUtils;

    /**
     * 消息列表
     *
     * @return
     */
    @RequestMapping(value = {"list"})
    public String list(@RequestParam(defaultValue = "23380") String deviceSN) {

        Map<String, Object> params = new HashMap<>();
        params.put("deviceSN", deviceSN);

        return cwmUtils.dpGet("message/list", params);
    }

    /**
     * 消息列表
     *
     * @return
     */
    @RequestMapping(value = {"show"})
    public String show(@RequestParam(defaultValue = "319502") String msgId) {

        Map<String, Object> params = new HashMap<>();
        params.put("msgId", msgId);

        return cwmUtils.dpGet("message/show", params);
    }

}
